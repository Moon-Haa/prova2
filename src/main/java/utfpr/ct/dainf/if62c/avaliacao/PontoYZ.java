/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.avaliacao;

/**
 *
 * @author Moon-Haa
 */
public class PontoYZ extends Ponto2D{
    
    public PontoYZ(){
        super();
    }
    
    public PontoYZ(double a, double b){
        super(0.0,a,b);
    }
    
    @Override
    public String toString(){
        return String.format("%s(%f,%f)",this.getNome(),this.y,this.z);
    }
    
}
