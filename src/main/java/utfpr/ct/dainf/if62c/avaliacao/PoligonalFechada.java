/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.avaliacao;

/**
 *
 * @author Moon-Haa
 */
public class PoligonalFechada extends Poligonal {
    protected Ponto2D vertices[];
    
    public PoligonalFechada(int a){
        super(a);
    }
    
    @Override
    public double getComprimento(){
        int i;
        double distancia = 0;
        for(i = 0; i<nVertices; i++){
            distancia += vertices[i].dist(vertices[i+1]);
        }
        distancia += vertices[i].dist(vertices[0]);
        return distancia;
    }
}
