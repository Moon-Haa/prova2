/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.avaliacao;

/**
 *
 * @author Moon-Haa
 */
public class PontoXY extends Ponto2D {
    
    public PontoXY(){
        super();
    }
    
    public PontoXY(double a, double b){
        super(a,b,0.0);
    }
    
    @Override
    public String toString(){
        return String.format("%s(%f,%f)",this.getNome(),this.x,this.y);
    }
}
