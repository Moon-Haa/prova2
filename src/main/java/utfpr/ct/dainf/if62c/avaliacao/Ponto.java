package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Segunda avaliação parcial 2014/2.
 * @author 
 */
public class Ponto {
    protected double x, y, z;

    public Ponto()
    {
        x = 0.0;
        y = 0.0;
        z = 0.0;
    }
    
    public Ponto(double a, double b, double c){
        x = a;
        y = b;
        z = c;
    }
    /**
     * Retorna no nome não-qualificado da classe.
     * @return O nome não qualificado da classe.
     */
    public String getNome() {
        return getClass().getSimpleName();
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
        hash = 79 * hash + (int) (Double.doubleToLongBits(this.z) ^ (Double.doubleToLongBits(this.z) >>> 32));
        return hash;
    }

    @Override
    public String toString(){
        return String.format("%s(%f,%f,%f)",this.getNome(),this.x,this.y,this.z);
    } 
    
    public boolean equals (Ponto pto)
    {
        try{
            if( (pto.getX() == this.x) && (pto.getY() == this.y) && (pto.getZ() == this.z) )
                return true;
            else
                return false;
        }
        catch (Exception e){
            return false;
        }
    }
    
    public double dist(Ponto pto){
        double a = pto.getX() - this.x;
        double b = pto.getY() - this.y;
        double c = pto.getZ() - this.z;
        return Math.sqrt(a*a + b*b + c*c);
    }
}
