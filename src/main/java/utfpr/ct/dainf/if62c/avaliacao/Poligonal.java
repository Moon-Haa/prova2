/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.avaliacao;

/**
 *
 * @author Moon-Haa
 */
public class Poligonal {
    private Ponto2D vertices[];
    protected int nVertices;
    protected String tipo;
      
    public Poligonal(int n){
        tipo = "";
        nVertices = n;
        if(n < 3){
            throw new RuntimeException("Poligonal deve ter ao menos 2 vértices");
        }
        else
            vertices = new Ponto2D[n];
    }
    
    public int getN(){
        return nVertices;
    }
    
    public Ponto2D get(int n){
        if ( (n > (nVertices-1)) || (n < 1) )
            return null;
        else
            return vertices[n];
    }
    
    public void set(int n, Ponto2D pto)
    {
        if ( (n<1) || (n> (nVertices-1)))
            return;
        
        if("".equals(tipo))
        {
            tipo = pto.getNome();
        }
        else{
            if(pto.getNome().equals(tipo))
            {
                vertices[n] = pto;
            }
            else
                throw new RuntimeException("Vértices devem estar no mesmo plano");
        }
    }
    
    public double getComprimento(){
        int i;
        double distancia = 0;
        for(i = 0; i<nVertices; i++){
            distancia += vertices[i].dist(vertices[i+1]);
        }
        return distancia;
    }
}
